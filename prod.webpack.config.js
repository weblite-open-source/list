const { resolve } = require('path');
const webpack = require('webpack');

var FlowStatusWebpackPlugin = require('flow-status-webpack-plugin');
var DashboardPlugin = require('webpack-dashboard/plugin');

module.exports = function(env) {
  return {
    context: resolve(__dirname + '/src'),

    entry: {
        index: './scripts/setup/prod.index.js',
    },

    output: {
      filename: '[name].js',
      path: resolve(__dirname, 'npm'),
      publicPath: '/',
      libraryTarget: 'umd',
    },

    devtool: 'nosources-source-map',

    resolve: {
      extensions: [".js", ".jsx"]
    },

    module: {
      rules: [
        {
          enforce: "pre",
          test: /\.(jsx|js)$/,
          exclude: /node_modules/,
          loader: "eslint-loader",
        },
        {
          test: /\.(jsx|js)?$/,
          use: [ 'babel-loader', ],
          exclude: /node_modules/
        },
      ],
    },

    externals: {
      "react": {
        commonjs: "react",
        commonjs2: "react",
        amd: "react",
        umd: "react",
        root: "React",
      },
      "react-dom": {
        commonjs: "react-dom",
        commonjs2: "react-dom",
        amd: "react-dom",
        umd: "react-dom",
        root: "ReactDom",
      },
      "prop-types": {
        commonjs: "prop-types",
        commonjs2: "prop-types",
        amd: "prop-types",
        umd: "prop-types",
      },
      "ramda": {
        commonjs: "ramda",
        commonjs2: "ramda",
        amd: "ramda",
        umd: "ramda",
        root: "R",
      },
      "material-ui/Paper": {
        commonjs: "material-ui/Paper",
        commonjs2: "material-ui/Paper",
        amd: "material-ui/Paper",
        umd: "material-ui/Paper",
      },
      "material-ui/List": {
        commonjs: "material-ui/List",
        commonjs2: "material-ui/List",
        amd: "material-ui/List",
        umd: "material-ui/List",
      },
      "material-ui/Avatar": {
        commonjs: "material-ui/Avatar",
        commonjs2: "material-ui/Avatar",
        amd: "material-ui/Avatar",
        umd: "material-ui/Avatar",
      },
    },

    plugins: [
      new webpack.HotModuleReplacementPlugin(),

      new webpack.NamedModulesPlugin(),

      new webpack.DefinePlugin({
        'process.env': {
          'NODE_ENV': JSON.stringify('production')
        }
      }),

      new FlowStatusWebpackPlugin({
        failOnError: true,
      }),

      new DashboardPlugin({ port: 9002 }),
    ],
  }
};
