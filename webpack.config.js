function buildConfig(env) {
  return require('./' + env + '.webpack.config.js')(env)
}

module.exports = buildConfig;
