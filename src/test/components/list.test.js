import React from 'react'
import { shallow, mount } from 'enzyme'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { ListItem } from 'material-ui/List'
import Avatar from 'material-ui/Avatar'
import injectTapEventPlugin from 'react-tap-event-plugin'

import List from '../../scripts/components/list'
import ChatListItem from '../../scripts/components/listitem'

beforeAll(() => {
  injectTapEventPlugin()
})

describe('<List />', () => {
  let wrapper
  let mockCallback = jest.fn()

  beforeAll(() => {
    const list = (
      <MuiThemeProvider>
        <List
          list={[
            { primaryText: 'ali', rightAvatarTextSelf: '3', rightAvatarSizeSelf: 25 },
            { primaryText: 'amir' }
          ]}
          onClick={mockCallback}
        />
      </MuiThemeProvider>
    )
    wrapper = mount(list)
  })

  it('first child of List must be ChatListItem', () => {
    // wrapper type is MuiThemeProvider
    expect(wrapper.children().first().name()).toBe('ChatListItem')
  })

  it('List must render 2 ChatListItem', () => {
    expect(wrapper.children().length).toBe(2)
  })

  it('shoud call mock 2 time when click on first listItem and second listItem', () => {
    // ChatListItem mount click on ListItem and ListItem mount click on span
    wrapper.children().first().find('span').simulate('click')
    wrapper.children().last().find('span').simulate('click')
    expect(mockCallback.mock.calls.length).toBe(2)
  })

  it('shoud print given primaryText property (amir) on second listItem', () => {
    expect(wrapper.children().last().text()).toBe('amir')
  })

  it('first List child must have right avatar with size of 25', () => {
    const firstElemRightAvatar = (
      <MuiThemeProvider>
        {wrapper.find(ListItem).first().props().rightAvatar}
      </MuiThemeProvider>
    )
    const avatarWrapper = shallow(firstElemRightAvatar)
    expect(avatarWrapper.prop('size')).toBe(25)
  })

})
