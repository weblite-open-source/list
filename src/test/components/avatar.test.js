import React from 'react'
import { shallow, mount } from 'enzyme'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import Avatar from '../../scripts/components/avatar'

describe('Avatar Creator Function', () => {
  let imgAvatar
  let txtAvatar
  let nullAvatar

  beforeAll(() => {
    imgAvatar = (
      <MuiThemeProvider>
        {Avatar('http://www.material-ui.com/images/kolage-128.jpg', '', 'white', 'black', 30)}
      </MuiThemeProvider>
    )

    txtAvatar = (
      <MuiThemeProvider>
        {Avatar('', 'AA', 'white', 'black', 30)}
      </MuiThemeProvider>
    )

    nullAvatar = Avatar('', '', 'white', 'black', 30)
  })

  it('shoud render image avatar when pass avatarSrc', () => {
    const wrapper = shallow(imgAvatar)
    expect(wrapper.prop('src')).toBe('http://www.material-ui.com/images/kolage-128.jpg')
  });

  it('(image avatar) shoud have size property of 30', () => {
    const wrapper = shallow(txtAvatar)
    expect(wrapper.prop('size')).toBe(30)
  });

  it('shoud render text avatar when not pass avatarSrc and pass avatarText', () => {
    const wrapper = mount(txtAvatar)
    expect(wrapper.text()).toBe('AA')
  });

  it('shoud render null when not pass avatarSrc or avatarText', () => {
    expect(nullAvatar).toBe(null)
  });
})
