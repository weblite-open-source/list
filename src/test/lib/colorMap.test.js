import { mapFirstCharToColor, mapCharsToColor, mapAfterSpaceCharToColor } from '../../scripts/lib/colorMap'


describe('mapFirstCharToColor functions:', () => {
  test('give it english string and must return hex color', () => {
    expect(mapFirstCharToColor('ali')).toMatchSnapshot()
  })

  test('give it farsi string and must return hex color', () => {
    expect(mapFirstCharToColor('علی')).toMatchSnapshot()
  })

  test('give it empty string and must return hex color', () => {
    expect(mapFirstCharToColor('')).toMatchSnapshot()
  })
})


describe('mapCharsToColor functions:', () => {
  test('give it english string and must return hex color', () => {
    expect(mapCharsToColor('ali')).toMatchSnapshot()
  })

  test('give it farsi string and must return hex color', () => {
    expect(mapCharsToColor('علی')).toMatchSnapshot()
  })

  test('give it empty string and must return hex color', () => {
    expect(mapCharsToColor('')).toMatchSnapshot()
  })
})


describe('mapAfterSpaceCharToColor functions:', () => {
  test('give it english string and must return hex color', () => {
    expect(mapAfterSpaceCharToColor('ali asgary')).toMatchSnapshot()
  })

  test('give it farsi string and must return hex color', () => {
    expect(mapAfterSpaceCharToColor('علی عسگری')).toMatchSnapshot()
  })

  test('give it empty string and must return hex color', () => {
    expect(mapAfterSpaceCharToColor('')).toMatchSnapshot()
  })
})
