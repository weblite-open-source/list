import React from 'react'
import Avatar from 'material-ui/Avatar'


const ListAvatar = (
  avatarSrc,
  avatarText,
  avatarBackgroundColor,
  avatarColor,
  avatarSize,
) => {
  if (!avatarText && !avatarSrc) return null
  else if (avatarSrc) {
    return (
      <Avatar
        size={avatarSize}
        src={avatarSrc}
      />
    )
  }
  return (
    <Avatar
      size={avatarSize}
      color={avatarColor}
      backgroundColor={avatarBackgroundColor}
    >
      {avatarText}
    </Avatar>
  )
}


export default ListAvatar
