import React from 'react'
import PropTypes from 'prop-types'
import { ListItem } from 'material-ui/List'
import ChatAvatar from './avatar'

const styles = {
  listItem: {
    color: 'white',
    marginBottom: 5,
  },
  secondaryText: {
    fontWeight: 100,
    fontSize: 14,
  },
}

const ChatListItem = ({
  index,
  onClick,
  id,
  primaryText,
  primaryTextColor,
  secondaryText,
  secondaryTextColor,
  hoverColor,

  leftAvatarSrc,
  leftAvatarText,
  leftAvatarBackgroundColor,
  leftAvatarColor,
  leftAvatarSize,

  rightAvatarSrc,
  rightAvatarText,
  rightAvatarBackgroundColor,
  rightAvatarColor,
  rightAvatarSize,
}) => (
  <ListItem
    style={{
      ...styles.listItem,
      color: primaryTextColor,
    }}
    primaryText={primaryText}
    secondaryText={
      secondaryText ?
      (
        <div
          style={{
            ...styles.secondaryText,
            color: secondaryTextColor,
          }}
        >
          {secondaryText}
        </div>
      ) : null
    }
    hoverColor={hoverColor}

    leftAvatar={
      ChatAvatar(
        leftAvatarSrc,
        leftAvatarText,
        leftAvatarBackgroundColor,
        leftAvatarColor,
        leftAvatarSize,
      )
    }

    rightAvatar={
      ChatAvatar(
        rightAvatarSrc,
        rightAvatarText,
        rightAvatarBackgroundColor,
        rightAvatarColor,
        rightAvatarSize,
      )
    }

    onClick={() => onClick({ id, index, primaryText })}
  />
)

ChatListItem.propTypes = {
  index: PropTypes.number.isRequired,
  onClick: PropTypes.func,
  primaryText: PropTypes.string.isRequired,
  primaryTextColor: PropTypes.string.isRequired,
  secondaryText: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.array,
    PropTypes.string,
  ]),
  secondaryTextColor: PropTypes.string.isRequired,
  hoverColor: PropTypes.string.isRequired,
  id: PropTypes.string,

  leftAvatarSrc: PropTypes.string,
  leftAvatarText: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  leftAvatarBackgroundColor: PropTypes.string.isRequired,
  leftAvatarColor: PropTypes.string.isRequired,
  leftAvatarSize: PropTypes.number.isRequired,

  rightAvatarSrc: PropTypes.string,
  rightAvatarText: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  rightAvatarBackgroundColor: PropTypes.string.isRequired,
  rightAvatarColor: PropTypes.string.isRequired,
  rightAvatarSize: PropTypes.number.isRequired,
}

ChatListItem.defaultProps = {
  secondaryText: null,
  onClick: () => {},
  id: '',

  leftAvatarSrc: '',
  leftAvatarText: '',

  rightAvatarSrc: '',
  rightAvatarText: '',
}

export default ChatListItem
