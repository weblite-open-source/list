// modules
import React from 'react'
import PropTypes from 'prop-types'
import { List } from 'material-ui/List'
// components
import ChatListItem from './listItem'


const ChatList = ({
  list,
  onClick,
  primaryTextColor,
  secondaryTextColor,
  hoverColor,

  leftAvatarBackgroundColor,
  leftAvatarColor,
  leftAvatarSize,

  rightAvatarBackgroundColor,
  rightAvatarColor,
  rightAvatarSize,
}) => (
  <List>
    {list.map((item, index) => (
      <ChatListItem
        key={item.id || index}
        primaryText={item.primaryText}
        primaryTextColor={primaryTextColor}
        secondaryText={item.secondaryText}
        secondaryTextColor={secondaryTextColor}
        hoverColor={hoverColor}
        id={item.id}
        onClick={onClick}
        index={index}

        leftAvatarSrc={item.leftAvatarSrcSelf}
        leftAvatarText={item.leftAvatarTextSelf}
        leftAvatarBackgroundColor={item.leftAvatarBackgroundColorSelf || leftAvatarBackgroundColor}
        leftAvatarColor={item.leftAvatarColorSelf || leftAvatarColor}
        leftAvatarSize={item.leftAvatarSizeSelf || leftAvatarSize}

        rightAvatarSrc={item.rightAvatarSrcSelf}
        rightAvatarText={item.rightAvatarTextSelf}
        rightAvatarBackgroundColor={
          item.rightAvatarBackgroundColorSelf ||
          rightAvatarBackgroundColor
        }
        rightAvatarColor={item.rightAvatarColorSelf || rightAvatarColor}
        rightAvatarSize={item.rightAvatarSizeSelf || rightAvatarSize}
      />
    ))}
  </List>
)


ChatList.propTypes = {
  primaryTextColor: PropTypes.string,
  secondaryTextColor: PropTypes.string,
  hoverColor: PropTypes.string,

  list: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    primaryText: PropTypes.string,
    secondaryText: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.array,
      PropTypes.string,
    ]),

    leftAvatarSrcSelf: PropTypes.string,
    leftAvatarTextSelf: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
    ]),
    leftAvatarBackgroundColorSelf: PropTypes.string,
    leftAvatarColorSelf: PropTypes.string,
    leftAvatarSizeSelf: PropTypes.number,

    rightAvatarSrcSelf: PropTypes.string,
    rightAvatarTextSelf: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
    ]),
    rightAvatarBackgroundColorSelf: PropTypes.string,
    rightAvatarColorSelf: PropTypes.string,
    rightAvatarSizeSelf: PropTypes.number,
  })),

  leftAvatarBackgroundColor: PropTypes.string,
  leftAvatarColor: PropTypes.string,
  leftAvatarSize: PropTypes.number,

  rightAvatarBackgroundColor: PropTypes.string,
  rightAvatarColor: PropTypes.string,
  rightAvatarSize: PropTypes.number,

  onClick: PropTypes.func,
}

ChatList.defaultProps = {
  primaryTextColor: 'black',
  secondaryText: '',
  secondaryTextColor: '#9E9E9E',
  list: [],
  hoverColor: '#BDBDBD',

  leftAvatarBackgroundColor: '#2196F3',
  leftAvatarColor: '#FFFFFF',
  leftAvatarSize: 40,

  rightAvatarBackgroundColor: '#2196F3',
  rightAvatarColor: '#FFFFFF',
  rightAvatarSize: 40,

  onClick: () => {},
}

export default ChatList
