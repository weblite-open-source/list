import List from '../components/list'
import { mapFirstCharToColor, mapCharsToColor, mapAfterSpaceCharToColor } from '../lib/colorMap'

export {
  List,
  mapFirstCharToColor,
  mapCharsToColor,
  mapAfterSpaceCharToColor,
}
