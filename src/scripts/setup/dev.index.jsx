import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import injectTapEventPlugin from 'react-tap-event-plugin'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import Paper from 'material-ui/Paper'

import List from '../components/list'
import { mapCharsToColor } from '../lib/colorMap'

injectTapEventPlugin()

const styles = {
  paper: {
    width: 300,
    height: 500,
    background: '#424242',
    margin: '0 auto',
    marginTop: 50,
  },
}

const render = () => {
  ReactDOM.render(
    <AppContainer>
      <MuiThemeProvider>
        <Paper style={styles.paper}>
          <List
            list={[
              {
                primaryText: 'ali asgary',
                secondaryText: 'salam va dorod be mahzare jenab aWli',
                leftAvatarTextSelf: 'AA',
                leftAvatarBackgroundColorSelf: mapCharsToColor('ali asgar'),
                rightAvatarTextSelf: '56',
              },
              {
                primaryText: 'eric hoffman',
                secondaryText: 'chetori chikara mikoni??',
                leftAvatarSrcSelf: 'http://www.material-ui.com/images/kolage-128.jpg',
                rightAvatarTextSelf: '3',
              },
              {
                primaryText: 'amirhossein shafiee',
                leftAvatarTextSelf: 'AS',
                leftAvatarBackgroundColorSelf: mapCharsToColor('amirhosein shafiee'),
                rightAvatarTextSelf: 100,
              },
              {
                primaryText: 'loe jing',
                secondaryText: 'سلام عزیزم، عزیزم سلام',
                leftAvatarSrcSelf: 'http://www.material-ui.com/images/uxceo-128.jpg',
              },
            ]}
            rightAvatarSize={20}
            hoverColor="blue"
            primaryTextColor="white"
            secondaryTextColor="black"
            onClick={item => console.log(item)}
          />
        </Paper>
      </MuiThemeProvider>
    </AppContainer>,
    document.getElementById('root'),
  )
}

render()

// Hot Module Replacement API
if (module.hot) {
  module.hot.accept('../components/list', () => {
    render()
  })
}
