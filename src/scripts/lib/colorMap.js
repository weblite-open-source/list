// @flow


// modules
import R from 'ramda'
// constants
import colors from '../constants/colorDB'


// type aliases
type colorHEX = string


// functions
const mapFirstCharToColor = (text: string): colorHEX =>
  R.compose(
    R.nth(R.__, colors),
    R.modulo(R.__, colors.length),
    R.reduce((acc, val) => R.bind(''.charCodeAt)(val)() + acc, 0),
    R.head,
  )(text)

const mapCharsToColor = (text: string): colorHEX =>
  R.compose(
    R.nth(R.__, colors),
    R.modulo(R.__, colors.length),
    R.reduce((acc, val) => R.bind(''.charCodeAt)(val)() + acc, 0),
    R.split(''),
  )(text)

const mapAfterSpaceCharToColor = (text: string): colorHEX =>
  R.compose(
    R.nth(R.__, colors),
    R.modulo(R.__, colors.length),
    R.reduce((acc, val) => R.bind(''.charCodeAt)(val)() + acc, 0),
    R.map(str => str[0]),
    R.filter(str => str),
    R.split(' '),
  )(text)


export { mapFirstCharToColor, mapCharsToColor, mapAfterSpaceCharToColor }
